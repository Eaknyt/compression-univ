#include <algorithm>
#include <assert.h>
#include <cmath>
#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <vector>

#include "common/color.h"
#include "common/colorspaces.h"
#include "common/image.h"
#include "common/psnr.h"
#include "common/utils.h"
#include "common/project_utils.h"


bool fuzzyEquals(unsigned char n1, unsigned char n2, unsigned char step)
{
    return std::abs(n2 - n1) <= step;
}

bool isFuzzyGray(const Color &color)
{
    const unsigned char step = 3;

    const unsigned char r = color.red();
    const unsigned char g = color.green();
    const unsigned char b = color.blue();

    if (r == g && g == b) {
        return false;
    }

    if (r == g && fuzzyEquals(r, b, step)) {
        return true;
    }

    if (r == b && (fuzzyEquals(r, g, step))) {
        return true;
    }

    if (g == b && fuzzyEquals(r, g, step)) {
        return true;
    }

    return false;
}

Color roundGray(const Color &color)
{
    Color ret;

    const unsigned char r = color.red();
    const unsigned char g = color.green();
    const unsigned char b = color.blue();

    if (r != g && r != b) {
        ret = Color::fromRgb(g, g, b);
    }

    if (g != b && g != r) {
        ret = Color::fromRgb(r, r, b);
    }

    if (b != r && b != g) {
        ret = Color::fromRgb(r, g, g);
    }

    return ret;
}

Image quantif(const Image &img)
{
    Image ret = img;

    for (int y = 0; y < ret.height(); ++y) {
        for (int x = 0; x < ret.width(); ++x) {
            Color px = ret.pixel(x, y);

            if (isFuzzyGray(px)) {
                const Color roundedPx = roundGray(px);

                ret.setPixel(x, y, Color::fromRgb(128, 128, 128));
            }
        }
    }

    return ret;
}

int main(int argc, char **argv)
{
    if (argc < 2) {
        std::cerr << "Usage : <imgPath_1>  ... <imgPath_n> k" << std::endl;
        return 1;
    }

    const int i = 3;

    for (int argI = 1; argI < argc; ++argI) {
        const std::string imgPath = argv[argI];

        Image img;

        if (!img.read(imgPath)) {
            std::cerr << imgPath << " : invalid image path." << std::endl;
            continue;
        }

        if (img.width() != img.height()) {
            std::cerr << imgPath << " : is not a square image." << std::endl;
            continue;
        }

        const std::string imgName = baseName(imgPath);
        const std::string ext = (img.isGrayscale() ? ".pgm" : ".ppm");

        const bool imgIsColored = img.isColored();

        Image imgToProcess = img;

        if (imgIsColored) {
            imgToProcess = rgbToYCbCr(img);
        }

        // Wavelet transform
        Image result = haarWavelet(imgToProcess, i, i);
        result = quantif(result);

        result.write("res/processed/" + imgName + ext);

        // Inverse transform
        Image resultBack = inverseHaarWavelet(result, i, i);

        if (imgIsColored) {
            resultBack = yCbCrToRgb(resultBack);
        }

        resultBack.write("res/processed/" + imgName + "_back_proj" + ext);

        std::cout << psnr(img, resultBack) << std::endl;

        const std::string huffmanCmd =
                "./huffman_tool c res/processed/" + imgName + ext +
                " res/processed/comp_" + imgName + ext;
        system(huffmanCmd.data());
    }

    return 0;
}
