#include <algorithm>
#include <assert.h>
#include <fstream>
#include <iostream>
#include <string>

#include "common/color.h"
#include "common/image.h"





std::vector<Color> colorTable(const Image &img)
{
    std::vector<Color> ret;
    ret.reserve(256);

    for (int y = 0; y < img.height(); ++y) {
        for (int x = 0; x < img.width(); ++x) {
            const Color color = img.pixel(x, y);

            if (std::find(ret.begin(), ret.end(), color) == ret.end()) {
                ret.push_back(color);
            }
        }
    }

    return ret;
}

void exportPalette(const std::vector<Color> table,
                   const std::string &outputFilePath)
{
    std::ofstream paletteOfs(outputFilePath);

    for (int i = 0; i < table.size(); ++i) {
        const Color color = table.at(i);
        paletteOfs << i << " "
                   << (int) color.red() << ","
                   << (int) color.green() << ","
                   << (int) color.blue()
                   << std::endl;
    }
}

void exportPalettedImage(const Image &img, const std::vector<Color> table,
                         const std::string &outputFilePath)
{
    const int imgWidth = img.width();
    const int imgHeight = img.height();

    Image palettedImg(imgWidth, imgHeight, Image::Format::Grayscale8);

    for (int y = 0; y < imgHeight; ++y) {
        for (int x = 0; x < imgWidth; ++x) {
            auto colorFoundInTable = std::find(table.begin(), table.end(),
                                               img.pixel(x, y));

            assert (colorFoundInTable != table.end());

            const unsigned char colorIdx = std::distance(table.begin(),
                                                         colorFoundInTable);

            palettedImg(x, y) = colorIdx;
        }
    }

    palettedImg.write(outputFilePath);
}


int main(int argc, char **argv)
{
    if (argc < 2) {
        std::cerr << "Usage : <imgPath_1> ... <imgPath_n>" << std::endl;
        return 1;
    }

    for (int argI = 1; argI < argc; ++argI) {
        const std::string imgPath = argv[argI];

        Image img;

        if (!img.read(imgPath)) {
            std::cerr << imgPath << " : invalid image path." << std::endl;
            return 1;
        }

        if (img.format() != Image::Format::Rgb888) {
            std::cerr << imgPath << " is not an rgb image" << std::endl;
            return 1;
        }

        std::cout << "Processing " << imgPath << std::endl;

        const std::vector<Color> palette = colorTable(img);

        // Write final image
        const std::size_t baseNameBeginIdx = imgPath.find_last_of('/') + 1;
        const std::size_t baseNameEndIdx = imgPath.find_last_of('.') - baseNameBeginIdx;

        const std::string baseImgName = imgPath.substr(baseNameBeginIdx,
                                                       baseNameEndIdx);

        const std::string outputPaletteFilePath =
                "res/processed/" + baseImgName + "_palette.txt";
        const std::string outputPalettedImgFilePath =
                "res/processed/" + baseImgName + "_paletted.pgm";

        exportPalette(palette, outputPaletteFilePath);
        exportPalettedImage(img, palette, outputPalettedImgFilePath);
    }

    return 0;
}
