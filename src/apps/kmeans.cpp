#include <algorithm>
#include <assert.h>
#include <cmath>
#include <iostream>
#include <random>
#include <string>

#include "common/color.h"
#include "common/image.h"
#include "common/psnr.h"




constexpr float MAX_RGB_DISTANCE = 441.67295593;


float colorDistance(const Color &c1, const Color &c2)
{
    return std::sqrt(std::pow(c1.red() - c2.red(), 2) +
                     std::pow(c1.green() - c2.green(), 2) +
                     std::pow(c1.blue() - c2.blue(), 2));
}

Color randomColor(const Image &img)
{
    // Choose 2 colors
    std::random_device rd;
    std::default_random_engine re(rd());

    std::uniform_int_distribution<int> widthDistrib(0, img.width() - 1);
    const int colorX = widthDistrib(re);

    std::uniform_int_distribution<int> heightDistrib(0, img.height() - 1);
    const int colorY = heightDistrib(re);

    const Color ret = img.pixel(colorX, colorY);

    return ret;
}

Image kMeansReduction(const Image &img, int k)
{
    // Classification
    std::vector<Color> initColors(k);
    std::vector<float> initColorsDistances(k / 2);

    auto computeInitColorsDistances = [&initColors, &initColorsDistances] ()
            -> std::vector<float> {
        std::vector<float> ret;

        for (int i = 0; i < initColors.size() - 1; i += 2) {
            const Color color1 = initColors.at(i);
            const Color color2 = initColors.at(i + 1);

            const float dist = colorDistance(color1, color2);

            ret.push_back(dist);
        }

        return ret;
    };

    auto convergenceReached = [] (const std::vector<float> oldDists,
                                  const std::vector<float> newDists) -> bool {
        const int size = oldDists.size();
        assert (size == newDists.size());

        for (int i = 0; i < size; ++i) {
            const float dist1 = oldDists.at(i);
            const float dist2 = newDists.at(i);

            if (dist2 - dist1 != 0) {
                return false;
            }
        }

        return true;
    };

    initColorsDistances = computeInitColorsDistances();

    std::generate(initColors.begin(), initColors.end(),
                  [img] { return randomColor(img); });

    std::vector<Color> means(k);
    std::vector<int> groupsCompsMeans(k * 3);
    std::vector<int> groupsColorCounts(k);

    Image meanImg = img;

    auto computeGroup = [&initColors] (const Color &color) -> int {
        float bestDistance = MAX_RGB_DISTANCE;
        int ret = -1;

        for (int i = 0; i < initColors.size(); ++i) {
            const float distance = colorDistance(color, initColors.at(i));

            if (distance < bestDistance) {
                bestDistance = distance;
                ret = i;
            }
        }

        assert (ret > -1);

        return ret;
    };

    auto findInitColorIndex = [&initColors] (const Color &color) -> int {
        auto it = std::find(initColors.begin(), initColors.end(), color);

        return std::distance(initColors.begin(), it);
    };

    while (true) {
        Image initImg = meanImg;

        // Make groups
        for (int y = 0; y < meanImg.height(); ++y) {
            for (int x = 0; x < meanImg.width(); ++x) {
                const Color color = meanImg.pixel(x, y);

                const int group = computeGroup(color);

                initImg.setPixel(x, y, initColors.at(group));

                groupsCompsMeans.at(group * 3) += color.red();
                groupsCompsMeans.at(group * 3 + 1) += color.green();
                groupsCompsMeans.at(group * 3 + 2) += color.blue();

                groupsColorCounts.at(group)++;
            }
        }

        // Compute groups means
        for (int i = 0; i < groupsCompsMeans.size(); ++i) {
            int &meanComp = groupsCompsMeans.at(i);

            const int group = i / 3;
            const int colorCount = groupsColorCounts.at(group);
            (colorCount) ? meanComp /= colorCount : meanComp = 0;
        }

        for (int i = 0; i < means.size(); ++i) {
            means.at(i) = Color::fromRgb(groupsCompsMeans.at(i * 3),
                                         groupsCompsMeans.at(i * 3 + 1),
                                         groupsCompsMeans.at(i * 3 + 2));
        }

        initColors = means;

        // Compose mean image
        for (int y = 0; y < initImg.height(); ++y) {
            for (int x = 0; x < initImg.width(); ++x) {
                const Color color = initImg.pixel(x, y);

                const int idx = findInitColorIndex(color);
                if (idx != initColors.size()) {
                    meanImg.setPixel(x, y, means.at(idx));
                }
            }
        }

        // Check for convergence
        const std::vector<float> newColorsDistances = computeInitColorsDistances();

        if (convergenceReached(initColorsDistances, newColorsDistances)) {
            break;
        }

        // Reinit helpers
        initColorsDistances = newColorsDistances;

        std::fill(groupsCompsMeans.begin(), groupsCompsMeans.end(), 0);
        std::fill(groupsColorCounts.begin(), groupsColorCounts.end(), 0);
    }

    return meanImg;
}


int main(int argc, char **argv)
{
    if (argc < 3) {
        std::cerr << "Usage : <k> <imgPath_1> ... <imgPath_n>" << std::endl;
        return 1;
    }

    const int k = std::stoi(argv[1]);

    if (k % 2 != 0) {
        std::cerr << "k must be an even number." << std::endl;
        return 1;
    }

    for (int argI = 2; argI < argc; ++argI) {
        const std::string imgPath = argv[argI];

        Image img;

        if (!img.read(imgPath)) {
            std::cerr << imgPath << " : invalid image path." << std::endl;
            return 1;
        }

        if (img.format() != Image::Format::Rgb888) {
            std::cerr << imgPath << " is not an rgb image" << std::endl;
            return 1;
        }

        std::cout << "Processing " << imgPath
                  << " using " << std::to_string(k) << " clusters..."
                  << std::endl;

        const Image result = kMeansReduction(img, k);

        // Write final image
        const std::string ext = ".ppm";

        const std::size_t baseNameBeginIdx = imgPath.find_last_of('/') + 1;
        const std::size_t baseNameEndIdx = imgPath.find_last_of('.') - baseNameBeginIdx;

        const std::string baseImgName = imgPath.substr(baseNameBeginIdx,
                                                       baseNameEndIdx);

        const std::string outputFilePath =
                "res/processed/" + baseImgName + "_" +
                std::to_string(k) + "mean" + ext;

        result.write(outputFilePath);

        std::cout << "Result saved as " << outputFilePath << std::endl;
        std::cout << "psnr = " << psnr(img, result) << std::endl;
    }

    return 0;
}
