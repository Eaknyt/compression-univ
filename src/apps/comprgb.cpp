#include <algorithm>
#include <assert.h>
#include <cmath>
#include <iostream>
#include <random>
#include <string>

#include "common/channels.h"
#include "common/color.h"
#include "common/colorspaces.h"
#include "common/image.h"
#include "common/psnr.h"
#include "common/resampling.h"


Image reduceAndResample(const Image &img, bool ycrcb)
{
    const int width = img.width();
    const int height = img.height();
    const int halfWidth = width / 2;
    const int halfHeight = height / 2;

    // Choose two channels
    const std::array<Image, 2> channelImages {
        channel(img, (ycrcb) ? 1 : 0),
        channel(img, (ycrcb) ? 2 : 2)
    };

    std::array<Image, 2> resampledChannels;

    for (int i = 0; i < channelImages.size(); i++) {
        const Image channelImg = channelImages.at(i);

        // Reduce
        Image reducedImg(halfWidth, halfHeight, Image::Format::Grayscale8);

        for (int y = 0; y < height; y += 2) {
            for (int x = 0; x < width; x += 2) {
                reducedImg(x / 2, y / 2) = channelImg(x, y);
            }
        }

        // Re-sample
        resampledChannels.at(i) = resampleBilinear(reducedImg, width, height);
    }

    // Assemble
    Image ret = assemble((ycrcb) ? channel(img, 0) : resampledChannels.at(0),
                         (ycrcb) ? resampledChannels.at(0) : channel(img, 1),
                         resampledChannels.at(1));
    return ret;
}


int main(int argc, char **argv)
{
    if (argc < 2) {
        std::cerr << "Usage : <imgPath_1>  ... <imgPath_n>" << std::endl;
        return 1;
    }

    for (int argI = 1; argI < argc; ++argI) {
        const std::string imgPath = argv[argI];

        Image img;

        if (!img.read(imgPath)) {
            std::cerr << imgPath << " : invalid image path." << std::endl;
            return 1;
        }

        if (img.format() != Image::Format::Rgb888) {
            std::cerr << imgPath << " is not an rgb image" << std::endl;
            return 1;
        }

        // Process
        Image rgbResult = reduceAndResample(img, false);
        Image ycbcrResult = yCbCrToRgb(reduceAndResample(rgbToYCbCr(img), true));

        // Write final image
        const std::string ext = ".ppm";

        const std::size_t baseNameBeginIdx = imgPath.find_last_of('/') + 1;
        const std::size_t baseNameEndIdx = imgPath.find_last_of('.') - baseNameBeginIdx;

        const std::string baseImgName = imgPath.substr(baseNameBeginIdx,
                                                       baseNameEndIdx);

        const std::string rgbOutputFilePath =
                "res/processed/" + baseImgName + "_rgb_comp" + ext;
        const std::string ycbcrbOutputFilePath =
                "res/processed/" + baseImgName + "_ycbcr_comp" + ext;

        rgbResult.write(rgbOutputFilePath);
        ycbcrResult.write(ycbcrbOutputFilePath);

        // Compute PSNR
        std::cout << "psnr for rgb = " << psnr(img, rgbResult) << std::endl;
        std::cout << "psnr for ycbcr = " << psnr(img, ycbcrResult) << std::endl;
    }

    return 0;
}
