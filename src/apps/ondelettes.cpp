#include <algorithm>
#include <assert.h>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include "common/channels.h"
#include "common/colorspaces.h"
#include "common/image.h"
#include "common/psnr.h"
#include "common/utils.h"

#include "eplot/plot.h"



using namespace eplot;


Image haarWavelet(const Image& img, int k, int q)
{
    const int imgWidth = img.width();
    const int imgHeight = img.height();

    assert (imgWidth == imgHeight);

    Image ret(imgWidth, imgHeight, img.format());
    const int iterSize = imgWidth / 2;

    const int qcoeff = k == 1 ? std::pow(2, q) : 1;

    for (int y = 0; y < iterSize; y++) {
        for (int x = 0; x < iterSize; x++) {
            for (int channel = 0; channel < img.channelCount(); channel++) {
                const int a = img(x * 2, y * 2, channel);
                const int b = img(x * 2 + 1, y * 2, channel);
                const int c = img(x * 2, y * 2 + 1, channel);
                const int d = img(x * 2 + 1, y * 2 + 1, channel);

                const int i = (a + b + c + d) / 4 * qcoeff;
                ret(x, y, channel) = i;

                const int newNextX = x + iterSize;
                const int newNextY = y + iterSize;

                const int j = clamp((a + b) / 2 - (c + d) / 2 + 128, 0, 255);
                ret(newNextX, y, channel) = j;

                const int k = clamp((a - b + c - d) / 2 + 128, 0, 255);
                ret(x, newNextY, channel) = k;

                const int l = clamp((a + d) - (b + c) + 128, 0, 255);
                ret(newNextX, newNextY, channel) = l;
            }
        }
    }

    if (k == 1) {
        return ret;
    }

    // /!\ Recursion
    const Image haar = haarWavelet(ret.subImage(0, 0, iterSize, iterSize), k - 1, q);

    for (int y = 0; y < haar.height(); y++) {
        for (int x = 0; x < haar.width(); x++) {
            for (int c = 0; c < haar.channelCount(); c++) {
                ret(x, y, c) = haar(x, y, c);
            }
        }
    }

    return ret;
}

Image inverseHaarWavelet(const Image &img, int k, int q)
{
    const int imgWidth = img.width();
    const int imgHeight = img.height();

    assert (imgWidth == imgHeight);

    Image ret = img;
    const int iterSize = imgWidth / std::pow(2, k);

    const int qcoeff = std::pow(2, q);

    for (int y = 0; y < iterSize; y++) {
        for (int x = 0; x < iterSize; x++) {
            const int x0 = x * 2;
            const int x1 = x * 2 + 1;
            const int x2 = x0;
            const int x3 = x1;

            const int y0 = y * 2;
            const int y1 = y0;
            const int y2 = y * 2 + 1;
            const int y3 = y2;

            for (int channel = 0; channel < img.channelCount(); channel++) {
                const int newNextX = x + iterSize;
                const int newNextY = y + iterSize;

                const int i = q > 0 ? img(x, y, channel) * qcoeff
                                    : img(x, y, channel);
                const int j = img(newNextX, y, channel) - 128;
                const int k = img(x, newNextY, channel) - 128;
                const int l = img(newNextX, newNextY, channel) - 128;

                const int d = clamp(i - (j + k) / 2 + (l / 4),    0, 255);
                const int a = clamp(j + k + d,                    0, 255);
                const int b = clamp(a - (l / 2) - k,              0, 255);
                const int c = clamp(a + d - b - l,                0, 255);

                ret(x0, y0, channel) = a;
                ret(x1, y1, channel) = b;
                ret(x2, y2, channel) = c;
                ret(x3, y3, channel) = d;
            }
        }
    }

    if (k == 1) {
        return ret;
    }

    // /!\ Recursion
    return inverseHaarWavelet(ret, k - 1, q);
}


int main(int argc, char **argv)
{
    if (argc < 3) {
        std::cerr << "Usage : <imgPath_1>  ... <imgPath_n> k" << std::endl;
        return 1;
    }

    const int k = std::atoi(argv[1]);

    for (int argI = 2; argI < argc; ++argI) {
        const std::string imgPath = argv[argI];

        Image img;

        if (!img.read(imgPath)) {
            std::cerr << imgPath << " : invalid image path." << std::endl;
            continue;
        }

        if (img.width() != img.height()) {
            std::cerr << imgPath << " : is not a square image." << std::endl;
            continue;
        }

        const std::string imgName = baseName(imgPath);
        const std::string ext = (img.isGrayscale() ? ".pgm" : ".ppm");

        const bool imgIsColored = img.isColored();

        Image imgToProcess = img;

        if (imgIsColored) {
            imgToProcess = rgbToYCbCr(img);
        }

        // Wavelet transform
        const int q = 0;

        Image result = haarWavelet(imgToProcess, k, q);
        result.write("res/processed/" + imgName + "_k" + std::to_string(k) + ext);

        // Inverse transform
        Image resultBack = inverseHaarWavelet(result, k, q);

        if (imgIsColored) {
            resultBack = yCbCrToRgb(resultBack);
        }

        resultBack.write("res/processed/" + imgName + "_back_k" + std::to_string(k) + ext);

        std::cout << psnr(img, resultBack) << std::endl;
    }

    return 0;
}
