#include <algorithm>
#include <assert.h>
#include <cmath>
#include <iostream>
#include <random>
#include <string>

#include "common/channels.h"
#include "common/color.h"
#include "common/histogram.h"
#include "common/image.h"
#include "common/psnr.h"
#include "common/resampling.h"
#include "common/utils.h"

#include "eplot/plot.h"



using namespace eplot;


Image diffMap(const Image &img)
{
    assert (img.format() == Image::Format::Grayscale8);

    Image ret = img;

    for (int y = 0; y < img.height(); y++) {
        for (int x = 1; x < img.width(); x++) {
            ret(x, y) = img(x, y) - img(x - 1, y) + 128;
        }
    }

    return ret;
}

Image reduceAndResample(const Image &img)
{
    const int width = img.width();
    const int height = img.height();
    const int halfWidth = width / 2;
    const int halfHeight = height / 2;

    Image reducedImg(halfWidth, halfHeight, Image::Format::Grayscale8);

    for (int y = 0; y < height; y += 2) {
        for (int x = 0; x < width; x += 2) {
            reducedImg(x / 2, y / 2) = img(x, y);
        }
    }

    Image ret = resampleBilinear(reducedImg, img.width(), img.height());

    return ret;
}


int main(int argc, char **argv)
{
    if (argc < 2) {
        std::cerr << "Usage : <imgPath_1>  ... <imgPath_n>" << std::endl;
        return 1;
    }

    for (int argI = 1; argI < argc; ++argI) {
        const std::string imgPath = argv[argI];

        Image img;

        if (!img.read(imgPath)) {
            std::cerr << imgPath << " : invalid image path." << std::endl;
            return 1;
        }

        if (img.format() != Image::Format::Grayscale8) {
            std::cerr << imgPath << " is not an grayscale image" << std::endl;
            return 1;
        }

        const std::string imgName = baseName(imgPath);

        plotHistogram(img, imgName);

        // Generate a difference map
        const Image outputImg = diffMap(img);

        outputImg.write("res/processed/" + imgName + "_diffmap.pgm");

        plotHistogram(outputImg, imgName + "_diffmap");

        // Reduce-resample-diffmap
        const Image resampledOutput = diffMap(reduceAndResample(img));
        resampledOutput.write("res/processed/" + imgName + "_resampled_diffmap.pgm");

        plotHistogram(resampledOutput, imgName + "_resampled_diffmap");
    }

    return 0;
}
