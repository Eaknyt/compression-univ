#ifndef H_COMMON_PSNR_H
#define H_COMMON_PSNR_H



class Image;

float psnr(const Image &img1, const Image &img2);



#endif // H_COMMON_PSNR_H
