#include "colorspaces.h"

#include "utils.h"




Image rgbToYCbCr(const Image &img)
{
    const int width = img.width();
    const int height = img.height();

    Image ret(width, height, Image::Format::Rgb888);

    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            const unsigned char r = img(x, y, 0);
            const unsigned char g = img(x, y, 1);
            const unsigned char b = img(x, y, 2);

            ret(x, y, 0) = 0.299 * r + 0.587 * g + 0.114 * b;
            ret(x, y, 1) = -0.168736 * r - 0.331264 * g + 0.5 * b + 128;
            ret(x, y, 2) = 0.5 * r - 0.418688 * g - 0.081312 * b + 128;
        }
    }

    return ret;
}

Image yCbCrToRgb(const Image &img)
{
    const int width = img.width();
    const int height = img.height();

    Image ret(width, height, Image::Format::Rgb888);

    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            const unsigned char lum = img(x, y, 0);
            const unsigned char cb = img(x, y, 1);
            const unsigned char cr = img(x, y, 2);

            ret(x, y, 0) = clamp(lum + 1.402 * (cr - 128), 0., 255.);
            ret(x, y, 1) = clamp(lum - 0.344136 * (cb - 128) - 0.714136 * (cr - 128), 0., 255.);
            ret(x, y, 2) = clamp(lum + 1.772 * (cb - 128), 0., 255.);
        }
    }

    return ret;
}


