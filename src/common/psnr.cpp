#include "psnr.h"

#include <assert.h>
#include <cmath>

#include "color.h"
#include "image.h"




float psnr(const Image &img1, const Image &img2)
{
    const int channelCount = img1.channelCount();

    assert (channelCount == img2.channelCount());

    const int imgWidth = img1.width();
    const int imgHeight = img1.height();

    // Compute MSE
    float eqm = 0.;

    for (int channelId = 0; channelId < channelCount; ++channelId) {
        float channelEqm = 0.;

        for (int y = 0; y < imgHeight; ++y) {
            for (int x = 0; x < imgWidth; ++x) {
                const unsigned char v1 = img1(x, y, channelId);
                const unsigned char v2 = img2(x, y, channelId);

                channelEqm += std::pow(v1 - v2, 2);
            }
        }

        channelEqm /= (imgWidth * imgHeight);

        eqm += channelEqm;
    }

    // Compute PSNR
    const float ret = 10 * std::log10(channelCount * std::pow(255, 2) / eqm);

    return ret;
}


