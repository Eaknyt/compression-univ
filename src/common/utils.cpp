#include "utils.h"

#include <algorithm>
#include <assert.h>
#include <numeric>

#include "histogram.h"
#include "../eplot/plot.h"

using namespace eplot;




std::string baseName(const std::string &path)
{
    const std::size_t baseNameBeginIdx = path.find_last_of('/') + 1;
    const std::size_t baseNameEndIdx = path.find_last_of('.') - baseNameBeginIdx;

    const std::string ret = path.substr(baseNameBeginIdx, baseNameEndIdx);

    return ret;
}

void plotHistogram(const Image &img, const std::string &imgName)
{
    assert (img.format() == Image::Format::Grayscale8);

    std::vector<double> points(256);
    std::iota(points.begin(), points.end(), 0);

    const std::vector<int> hist = histogram(img);
    const std::vector<double> histd(hist.begin(), hist.end());

    plotTitle("Histogram of " + imgName);
    plotLabels("Grayscale", "Occurrences");
    plot(points, histd, "#777777");
    plotWrite("res/processed/" + imgName);
}


