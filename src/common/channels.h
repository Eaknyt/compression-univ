#ifndef H_COMMON_CHANNEL_H
#define H_COMMON_CHANNEL_H

#include "image.h"



Image channel(const Image &img, int channel);

Image channels(const Image &img, int c1, int c2);

Image assemble(const Image &imgR, const Image &imgG, const Image &imgB);



#endif // H_COMMON_CHANNEL_H
