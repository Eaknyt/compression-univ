#include "channels.h"

#include <assert.h>
#include <iostream>


namespace {

void getChannel(unsigned char *pt_image, const unsigned char *src, int pxCount, int channel, int channelCount)
{
    for (int i = 0; i < pxCount; ++i){
        pt_image[i] = src[channelCount * i + channel];
    }
}

} // anon namespace




Image channel(const Image &img, int channel)
{
    const int channelCount = img.channelCount();

    assert (channel < channelCount);

    Image ret(img.width(), img.height(), Image::Format::Grayscale8);

    getChannel(ret.data(), img.data(), ret.pixelCount(), channel, channelCount);

    return ret;
}

Image channels(const Image &img, int c1, int c2)
{
    assert (c1 < img.channelCount());
    assert (c2 < img.channelCount());

    if (c1 == c2) {
        return Image();
    }

    int w = img.width();
    int h = img.height();
    const int finalImgSize = w * h * 3;

    const unsigned char *c1Data = channel(img, c1).data();
    const unsigned char *c2Data = channel(img, c2).data();

    std::vector<unsigned char> outputImgData(finalImgSize);

    int idx0 = static_cast<int>(c1);
    int idx1 = static_cast<int>(c2);
    int idx2 = 3 - idx0 - idx1;

    for (int i = 0; i < finalImgSize; i += 3) {
        int j = i / 3;

        outputImgData[i + idx0] = c1Data[j];
        outputImgData[i + idx1] = c2Data[j];
        outputImgData[i + idx2] = 0;
    }

    Image ret(outputImgData, img.width(), img.height(), Image::Format::Rgb888);

    return ret;
}

Image assemble(const Image &imgR, const Image &imgG, const Image &imgB)
{
    assert (imgR.width() == imgG.width() && imgG.width() == imgB.width());
    assert (imgR.height() == imgG.height() && imgG.height() == imgB.height());

    int w = imgR.width();
    int h = imgR.height();
    const int finalImgSize = w * h * 3;

    const unsigned char *oChannelData1 = imgR.data();
    const unsigned char *oChannelData2 = imgG.data();
    const unsigned char *oChannelData3= imgB.data();

    std::vector<unsigned char> outputImgData(finalImgSize);

    for (int i = 0; i < finalImgSize; i += 3) {
        int j = i / 3;

        outputImgData[i] = oChannelData1[j];
        outputImgData[i + 1] = oChannelData2[j];
        outputImgData[i + 2] = oChannelData3[j];
    }

    Image ret(outputImgData, w, h, Image::Format::Rgb888);

    assert (ret.size() == finalImgSize);

    return ret;
}


