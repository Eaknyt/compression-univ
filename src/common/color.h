#ifndef H_COMMON_COLOR_H
#define H_COMMON_COLOR_H

#include <iosfwd>




enum class GlobalColor : short
{
	Black,
	White,
	DarkGray,
	MediumGray,
	LightGray,
	Red,
	Green,
	Blue,
	Cyan,
	Magenta,
	Yellow,
	DarkRed,
	DarkGreen,
	DarkBlue,
	DarkCyan,
	DarkMagenta,
	DarkYellow,
	Transparent,
	XAxis = Red,
	YAxis,
	ZAxis
};

class Color
{
public:
	Color();
	Color(float r, float g, float b, float a = 1.);
	Color(GlobalColor color);
	Color(const Color &other);

	static Color fromRgb(unsigned char r, unsigned char g, unsigned char b,
		  				 unsigned char a = 255);
    void toRgb(unsigned char *r, unsigned char *g, unsigned char *b) const;

    unsigned char red() const;
    unsigned char green() const;
    unsigned char blue() const;
    unsigned char alpha() const;

    float redF() const;
    void setRedF(float r);

    float greenF() const;
    void setGreenF(float g);
	
    float blueF() const;
    void setBlueF(float b);

    float alphaF() const;
    void setAlphaF(float a);

	Color &operator=(const Color &color);
	Color &operator=(GlobalColor color);

    friend bool operator==(const Color &lhs, const Color &rhs);
    friend bool operator!=(const Color &lhs, const Color &rhs);

    friend const Color operator+(const Color &lhs, const Color &rhs);
    friend const Color operator*(const Color &lhs, float factor);

    friend std::ostream &operator<<(std::ostream &os, const Color &color);

private:
	float m_r;
	float m_g;
	float m_b;
	float m_a;
};



#endif // H_COMMON_COLOR_H
