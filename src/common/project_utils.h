#ifndef PROJECT_UTILS_h
#define PROJECT_UTILS_h

#include "image.h"


std::vector<Color> colorTable(const Image &img);
float colorDistance(const Color &c1, const Color &c2);
Color randomColor(const Image &img);
Image kMeansReduction(const Image &img, int k);
void exportPalette(const std::vector<Color> table,
                   const std::string &outputFilePath);
void exportPalettedImage(const Image &img, const std::vector<Color> table,
                         const std::string &outputFilePath);
Image reduceAndResample(const Image &img, bool ycrcb);
Image diffMap(const Image &img);
Image haarWavelet(const Image& img, int i, int oi);
Image inverseHaarWavelet(const Image &img, int i, int oi);

#endif // PROJECT_UTILS_h
