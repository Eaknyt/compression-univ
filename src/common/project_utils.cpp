#include "project_utils.h"

#include <algorithm>
#include <assert.h>
#include <cmath>
#include <fstream>
#include <random>
#include <unordered_set>


#include "color.h"
#include "channels.h"
#include "resampling.h"
#include "utils.h"


constexpr float MAX_RGB_DISTANCE = 441.67295593;


float colorDistance(const Color &c1, const Color &c2)
{
    return std::sqrt(std::pow(c1.red() - c2.red(), 2) +
                     std::pow(c1.green() - c2.green(), 2) +
                     std::pow(c1.blue() - c2.blue(), 2));
}

Color randomColor(const Image &img)
{
    // Choose 2 colors
    std::random_device rd;
    std::default_random_engine re(rd());

    std::uniform_int_distribution<int> widthDistrib(0, img.width() - 1);
    const int colorX = widthDistrib(re);

    std::uniform_int_distribution<int> heightDistrib(0, img.height() - 1);
    const int colorY = heightDistrib(re);

    const Color ret = img.pixel(colorX, colorY);

    return ret;
}

Image kMeansReduction(const Image &img, int k)
{
    // Classification
    std::vector<Color> initColors(k);
    std::vector<float> initColorsDistances(k / 2);

    auto computeInitColorsDistances = [&initColors, &initColorsDistances] ()
            -> std::vector<float> {
        std::vector<float> ret;

        for (int i = 0; i < initColors.size() - 1; i += 2) {
            const Color color1 = initColors.at(i);
            const Color color2 = initColors.at(i + 1);

            const float dist = colorDistance(color1, color2);

            ret.push_back(dist);
        }

        return ret;
    };

    auto convergenceReached = [] (const std::vector<float> oldDists,
                                  const std::vector<float> newDists) -> bool {
        const int size = oldDists.size();
        assert (size == newDists.size());

        for (int i = 0; i < size; ++i) {
            const float dist1 = oldDists.at(i);
            const float dist2 = newDists.at(i);

            if (dist2 - dist1 != 0) {
                return false;
            }
        }

        return true;
    };

    initColorsDistances = computeInitColorsDistances();

    std::generate(initColors.begin(), initColors.end(),
                  [img] { return randomColor(img); });

    std::vector<Color> means(k);
    std::vector<int> groupsCompsMeans(k * 3);
    std::vector<int> groupsColorCounts(k);

    Image meanImg = img;

    auto computeGroup = [&initColors] (const Color &color) -> int {
        float bestDistance = MAX_RGB_DISTANCE;
        int ret = -1;

        for (int i = 0; i < initColors.size(); ++i) {
            const float distance = colorDistance(color, initColors.at(i));

            if (distance < bestDistance) {
                bestDistance = distance;
                ret = i;
            }
        }

        assert (ret > -1);

        return ret;
    };

    auto findInitColorIndex = [&initColors] (const Color &color) -> int {
        auto it = std::find(initColors.begin(), initColors.end(), color);

        return std::distance(initColors.begin(), it);
    };

    while (true) {
        Image initImg = meanImg;

        // Make groups
        for (int y = 0; y < meanImg.height(); ++y) {
            for (int x = 0; x < meanImg.width(); ++x) {
                const Color color = meanImg.pixel(x, y);

                const int group = computeGroup(color);

                initImg.setPixel(x, y, initColors.at(group));

                groupsCompsMeans.at(group * 3) += color.red();
                groupsCompsMeans.at(group * 3 + 1) += color.green();
                groupsCompsMeans.at(group * 3 + 2) += color.blue();

                groupsColorCounts.at(group)++;
            }
        }

        // Compute groups means
        for (int i = 0; i < groupsCompsMeans.size(); ++i) {
            int &meanComp = groupsCompsMeans.at(i);

            const int group = i / 3;
            const int colorCount = groupsColorCounts.at(group);
            (colorCount) ? meanComp /= colorCount : meanComp = 0;
        }

        for (int i = 0; i < means.size(); ++i) {
            means.at(i) = Color::fromRgb(groupsCompsMeans.at(i * 3),
                                         groupsCompsMeans.at(i * 3 + 1),
                                         groupsCompsMeans.at(i * 3 + 2));
        }

        initColors = means;

        // Compose mean image
        for (int y = 0; y < initImg.height(); ++y) {
            for (int x = 0; x < initImg.width(); ++x) {
                const Color color = initImg.pixel(x, y);

                const int idx = findInitColorIndex(color);
                if (idx != initColors.size()) {
                    meanImg.setPixel(x, y, means.at(idx));
                }
            }
        }

        // Check for convergence
        const std::vector<float> newColorsDistances = computeInitColorsDistances();

        if (convergenceReached(initColorsDistances, newColorsDistances)) {
            break;
        }

        // Reinit helpers
        initColorsDistances = newColorsDistances;

        std::fill(groupsCompsMeans.begin(), groupsCompsMeans.end(), 0);
        std::fill(groupsColorCounts.begin(), groupsColorCounts.end(), 0);
    }

    return meanImg;
}

std::vector<Color> colorTable(const Image &img)
{
    std::vector<Color> ret;
    ret.reserve(256);

    for (int y = 0; y < img.height(); ++y) {
        for (int x = 0; x < img.width(); ++x) {
            const Color color = img.pixel(x, y);

            if (std::find(ret.begin(), ret.end(), color) == ret.end()) {
                ret.emplace_back(color);
            }
        }
    }

    return ret;
}

void exportPalette(const std::vector<Color> table,
                   const std::string &outputFilePath)
{
    std::ofstream paletteOfs(outputFilePath);

    for (int i = 0; i < table.size(); ++i) {
        const Color color = table.at(i);
        paletteOfs << i << " "
                   << (int) color.red() << ","
                   << (int) color.green() << ","
                   << (int) color.blue()
                   << std::endl;
    }
}

void exportPalettedImage(const Image &img, const std::vector<Color> table,
                         const std::string &outputFilePath)
{
    const int imgWidth = img.width();
    const int imgHeight = img.height();

    Image palettedImg(imgWidth, imgHeight, Image::Format::Grayscale8);

    for (int y = 0; y < imgHeight; ++y) {
        for (int x = 0; x < imgWidth; ++x) {
            auto colorFoundInTable = std::find(table.begin(), table.end(),
                                               img.pixel(x, y));

            assert (colorFoundInTable != table.end());

            const unsigned char colorIdx = std::distance(table.begin(),
                                                         colorFoundInTable);

            palettedImg(x, y) = colorIdx;
        }
    }

    palettedImg.write(outputFilePath);
}

Image reduceAndResample(const Image &img, bool ycrcb)
{
    const int width = img.width();
    const int height = img.height();
    const int halfWidth = width / 2;
    const int halfHeight = height / 2;

    // Choose two channels
    const std::array<Image, 2> channelImages {
        channel(img, (ycrcb) ? 1 : 0),
        channel(img, (ycrcb) ? 2 : 2)
    };

    std::array<Image, 2> resampledChannels;

    for (int i = 0; i < channelImages.size(); i++) {
        const Image channelImg = channelImages.at(i);

        // Reduce
        Image reducedImg(halfWidth, halfHeight, Image::Format::Grayscale8);

        for (int y = 0; y < height; y += 2) {
            for (int x = 0; x < width; x += 2) {
                reducedImg(x / 2, y / 2) = channelImg(x, y);
            }
        }

        // Re-sample
        resampledChannels.at(i) = resampleBilinear(reducedImg, width, height);
    }

    // Assemble
    Image ret = assemble((ycrcb) ? channel(img, 0) : resampledChannels.at(0),
                         (ycrcb) ? resampledChannels.at(0) : channel(img, 1),
                         resampledChannels.at(1));
    return ret;
}

Image diffMap(const Image &img)
{
    Image ret = img;

    for (int y = 0; y < img.height(); y++) {
        for (int x = 1; x < img.width(); x++) {
            for (int c = 0; c < img.channelCount(); c++) {
                ret(x, y, c) = img(x, y, c) - img(x - 1, y, c) + 128;
            }
        }
    }

    return ret;
}

Image haarWavelet(const Image& img, int i, int oi)
{
    const int imgWidth = img.width();
    const int imgHeight = img.height();

    assert (imgWidth == imgHeight);

    Image ret(imgWidth, imgHeight, img.format());
    const int iterSize = imgWidth / 2;

    for (int y = 0; y < iterSize; y++) {
        for (int x = 0; x < iterSize; x++) {
            for (int channel = 0; channel < img.channelCount(); channel++) {
                const int a = img(x * 2,        y * 2,      channel);
                const int b = img(x * 2 + 1,    y * 2,      channel);
                const int c = img(x * 2,        y * 2 + 1,  channel);
                const int d = img(x * 2 + 1,    y * 2 + 1,  channel);

                const int a2 = (a + b + c + d) / 4/* / qa2d*/;
                ret(x, y, channel) = clamp(a2, 0, 255);

                const int newNextX = x + iterSize;
                const int newNextY = y + iterSize;

                const float b2 = (a + b) / 2 - (c + d) / 2 + 128 /*/ qb2d*/;
                ret(newNextX, y, channel) = std::floor(clamp(int(b2), 0, 255));

                const float c2 = (a - b + c - d) / 2 + 128/* / qc2d*/;
                ret(x, newNextY, channel) = std::floor(clamp(int(c2), 0, 255));

                const float d2 = (a + d) - (b + c) + 128/* / qd2d*/;
                ret(newNextX, newNextY, channel) = std::floor(clamp(int(d2), 0, 255));
            }
        }
    }

    if (i == 1) {
        return ret;
    }

    // /!\ Recursion
    const Image haar = haarWavelet(ret.subImage(0, 0, iterSize, iterSize), i - 1, oi);

    for (int y = 0; y < haar.height(); y++) {
        for (int x = 0; x < haar.width(); x++) {
            for (int c = 0; c < haar.channelCount(); c++) {
                ret(x, y, c) = haar(x, y, c);
            }
        }
    }

    return ret;
}

Image inverseHaarWavelet(const Image &img, int i, int oi)
{
    const int imgWidth = img.width();
    const int imgHeight = img.height();

    assert (imgWidth == imgHeight);

    Image ret = img;
    const int iterSize = imgWidth / std::pow(2, i);

    for (int y = 0; y < iterSize; y++) {
        for (int x = 0; x < iterSize; x++) {
            const int x0 = x * 2;
            const int x1 = x * 2 + 1;
            const int x2 = x0;
            const int x3 = x1;

            const int y0 = y * 2;
            const int y1 = y0;
            const int y2 = y * 2 + 1;
            const int y3 = y2;

            for (int channel = 0; channel < img.channelCount(); channel++) {
                const int newNextX = x + iterSize;
                const int newNextY = y + iterSize;

                const int a2 = img(x, y, channel);
                const int b2 = img(newNextX, y, channel) - 128;
                const int c2 = img(x, newNextY, channel) - 128;
                const int d2 = img(newNextX, newNextY, channel) - 128;

                const float d = a2 - (b2 + c2) / 2 + (d2 / 4)/* * qd2d*/;
                const int a = b2 + c2 + d/* * qa2d*/;
                const float b = a - (d2 / 2) - c2/* * qb2d*/;
                const float c = a + d - b - d2/* * qc2d*/;

                ret(x0, y0, channel) = clamp(a, 0, 255);
                ret(x1, y1, channel) = clamp(int(b), 0, 255);
                ret(x2, y2, channel) = clamp(int(c), 0, 255);
                ret(x3, y3, channel) = clamp(int(d), 0, 255);
            }
        }
    }

    if (i == 1) {
        return ret;
    }

    // /!\ Recursion
    return inverseHaarWavelet(ret, i - 1, oi);
}
