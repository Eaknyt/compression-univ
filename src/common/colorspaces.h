#ifndef COLORSPACES_H
#define COLORSPACES_H

#include "image.h"




Image rgbToYCbCr(const Image &img);
Image yCbCrToRgb(const Image &img);



#endif // COLORSPACES_H
