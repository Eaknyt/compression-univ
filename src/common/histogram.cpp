#include "histogram.h"

#include <algorithm>
#include <assert.h>
#include <iostream>

#include "image.h"


namespace {


} // anon namespace



std::vector<int> histogram(const Image &img)
{
    std::vector<int> ret(256);

    if (img.format() != Image::Format::Rgb888) {
        for (int y = 0; y < img.height(); ++y) {
            for (int x = 0; x < img.width(); ++x) {
                auto xyValue = static_cast<int>(img(x, y));

                ret[xyValue]++;
            }
        }
    }

    return ret;
}

std::vector<int> histogram(const Image &img, int c)
{
    std::vector<int> ret(256);

    if (img.format() == Image::Format::Rgb888) {
        Image imgChannel = channel(img, c);

        for (int y = 0; y < imgChannel.height(); ++y) {
            for (int x = 0; x < imgChannel.width(); ++x) {

                auto xyValue = static_cast<int>(imgChannel(x, y));

                ret[xyValue]++;
            }
        }
    }

    return ret;
}

std::array<std::vector<int>, 3> histogramRgb(const Image &img)
{
    std::array<std::vector<int>, 3> ret;

    if (img.format() == Image::Format::Rgb888) {
        for (int i = 0; i < 3; ++i) {
            std::vector<int> channelHist = histogram(img, i);

            ret[i] = channelHist;
        }
    }

    return ret;
}

Image histExpand(const Image &img, float *a, float *b)
{
    assert (img.format() == Image::Format::Grayscale8);

    std::vector<int> histo = histogram(img);

    auto aMinIt = std::find_if(histo.begin(), histo.end(), [] (int val) {
        return val > 0;
    });

    auto aMaxIt = std::find_if(histo.rbegin(), histo.rend(), [] (int val) {
        return val > 0;
    });

    int aMin = std::distance(histo.begin(), aMinIt);
    int aMax = 255 - std::distance(histo.rbegin(), aMaxIt);

    float _a = 255 / (aMax - aMin);
    float _b = (-255 * aMin) / (aMax - aMin);

    Image ret = img;

    for (int y = 0; y < img.height(); ++y) {
        for (int x = 0; x < img.width(); ++x) {
            ret(x, y) = (255 / (aMax - aMin)) * (-aMin + ret(x, y));
        }
    }

    if (a) {
        *a = _a;
    }

    if (b) {
        *b = _b;
    }

    return ret;
}

std::vector<double> histCfd(const std::vector<int> &histo, int pixelCount)
{
    // Compute ddp
    std::vector<double> ddp(histo.begin(), histo.end());

    for (std::size_t i = 0; i < histo.size(); ++i) {
        ddp[i] = static_cast<double>(histo[i]) / pixelCount;
    }

    // Compute the distribution function
    std::vector<double> ret(ddp.size());
    ret[0] = ddp[0];

    for (std::size_t i = 1; i < ret.size(); ++i) {
        ret[i] = ret[i - 1] + ddp[i];
    }

    return ret;
}

Image histEqualize(const Image &img, const std::vector<double> &cfd)
{
    assert (img.format() == Image::Format::Grayscale8);

    const std::size_t N = 256;

    // Apply
    std::vector<double> T(N);

    for (std::size_t i = 0; i < N; ++i) {
        T[i] = 255 * cfd[i];
    }

    Image ret = img;

    for (int y = 0; y < img.height(); ++y) {
        for (int x = 0; x < img.width(); ++x) {
            ret(x, y) = T[ret(x, y)];
        }
    }

    return ret;
}


