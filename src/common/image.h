#ifndef H_COMMON_IMAGE_H
#define H_COMMON_IMAGE_H

#include <memory>
#include <string>
#include <vector>




struct ImagePrivate;

class Color;


class Image
{
public:
    enum class Format : short
    {
        Binary,
        Grayscale8,
        Rgb888,
    };

public:
    Image();
    Image(int w, int h, Format format);
    Image(const unsigned char *data, int w, int h, Image::Format format);
    Image(const std::vector<unsigned char> &data, int w, int h, Format format);
    Image(const std::string &filename);
    Image(const Image &other);
    ~Image();

    int width() const;
    int height() const;

    bool isNull() const;
    bool isValid() const;

    std::size_t size() const;
    int pixelCount() const;

    Format format() const;
    Image convert(Format f) const;

    bool isGrayscale() const;
    bool isColored() const;

    int channelCount() const;

    unsigned char *data();
    const unsigned char *data() const;

    std::vector<unsigned char *> row(int y);
    std::vector<unsigned char> row(int y) const;
    std::vector<unsigned char *> column(int x);
    std::vector<unsigned char> column(int x) const;

    Image subImage(int x, int y, int w, int h) const;

    Color pixel(int x, int y) const;
    void setPixel(int x, int y, const Color &color);

    void clear(unsigned char gray);
    void clear(unsigned char r, unsigned char g, unsigned char b);

    void copy(const Image &other);

    bool read(const std::string &filename);
    bool write(const std::string &filename) const;

    const unsigned char operator()(int x, int y) const;
    unsigned char &operator()(int x, int y);

    const unsigned char operator()(int x, int y, int channel) const;
    unsigned char &operator()(int x, int y, int channel);

    Image &operator=(const Image &other);

private:
    const std::unique_ptr<ImagePrivate> d;
};



#endif // H_COMMON_IMAGE_H
