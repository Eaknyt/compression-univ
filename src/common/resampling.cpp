#include "resampling.h"




Image resample(const Image& img, int newWidth, int newHeight)
{
    Image ret(newWidth, newHeight, img.format());

    for (int y = 0; y < newHeight; y++) {
        for (int x = 0; x < newWidth; x++) {
            const int oldX = (x * img.width()) / newWidth;
            const int oldY = (y * img.height()) / newHeight;

            ret(x, y) = img(oldX, oldY);
        }
    }

    return ret;
}

Image resampleBilinear(const Image &img, int newWidth, int newHeight)
{
    Image ret(newWidth, newHeight, img.format());

    const int oldWidth = img.width();
    const int oldHeight = img.height();

    for (int newY = 0; newY < newHeight; newY++) {
        for (int newX = 0; newX < newWidth; newX++) {
            const double x = (static_cast<double>(newX) * oldWidth) / newWidth;
            const double y = (static_cast<double>(newY) * oldHeight) / newHeight;

            const int xp1 = (x + 1 >= oldWidth ? x : x + 1);
            const int yp1 = (y + 1 >= oldHeight ? y : y + 1);

            const double delta = x - static_cast<int>(x);
            const double micro = y - static_cast<int>(y);

            const unsigned char p1 = img(x, y);
            const unsigned char p2 = img(xp1, y);
            const unsigned char p3 = img(x, yp1);
            const unsigned char p4 = img(xp1, yp1);

            ret(newX, newY) = (1 - delta) * (( 1 - micro) * p1 + micro * p3)
                    + delta * ( (1 - micro) * p2 + micro * p4);
        }
    }

    return ret;
}


