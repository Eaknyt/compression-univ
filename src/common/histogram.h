#ifndef H_COMMON_HISTOGRAM_H
#define H_COMMON_HISTOGRAM_H

#include <array>
#include <vector>

#include "channels.h"



class Image;

std::vector<int> histogram(const Image &img);
std::vector<int> histogram(const Image &img, int channel);

std::array<std::vector<int>, 3> histogramRgb(const Image &img);

Image histExpand(const Image &img, float *a = nullptr, float *b = nullptr);

std::vector<double> histCfd(const std::vector<int> &histo, int pixelCount);

Image histEqualize(const Image &img, const std::vector<double> &cfd);



#endif // H_COMMON_HISTOGRAM_H
