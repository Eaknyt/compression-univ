#ifndef RESAMPLING_H
#define RESAMPLING_H

#include "image.h"




Image resample(const Image& img, int newWidth, int newHeight);
Image resampleBilinear(const Image &img, int newWidth, int newHeight);



#endif // RESAMPLING_H
