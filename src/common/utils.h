#ifndef H_COMMON_UTILS_H
#define H_COMMON_UTILS_H

#include <string>

#include "image.h"




template <typename T>
T clamp(const T &value, const T &lowerBound, const T &higherBound)
{
    if (value < lowerBound) {
        return lowerBound;
    }

    if (value > higherBound) {
        return higherBound;
    }

    return value;
}

std::string baseName(const std::string &path);

void plotHistogram(const Image &img, const std::string &imgName);



#endif // H_COMMON_UTILS_H
